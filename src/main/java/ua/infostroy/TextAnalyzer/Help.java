package ua.infostroy.TextAnalyzer;

/**
 * Class is giving detailed information of how to use app.
 */
public class Help {

    private static final String INPUT = "-i (--input) - path to the input file " +
            "(e.g. C:\\Program Files\\Java\\input.txt)";
    private static final String TASK = "-t (--task) - task to execute. " +
            "Permitted values: frequency, length, duplicates";
    private static final String HELP = "--help - a detailed information " +
            "of how to use your app";

    public static void showMessage(){
        System.out.println(INPUT);
        System.out.println(TASK);
        System.out.println(HELP);
    }
}
