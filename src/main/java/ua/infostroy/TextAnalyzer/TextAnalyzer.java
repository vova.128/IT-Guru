package ua.infostroy.TextAnalyzer;

import java.io.*;
import java.util.*;

/**
 * Class containing the functions-tasks to execute: frequency, length, duplicates.
 */
public class TextAnalyzer {

    /**
     * Read the file into the char array.
     * @param path - the path to the file
     * @return String array of words
     */
    public String[] readFile(String path){
        File file = new File(path);
        char[] buffer = null;
        try{
            buffer = new char[(int)file.length()];
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(new FileInputStream(file), "UTF-8"));
            br.read(buffer);
            br.close();
        } catch (FileNotFoundException ex){
            System.err.println("File " + file.getName() + " wasn't found");
        } catch(IOException ex){
            System.out.println(ex.getMessage());
        }
        String[] words = new String(buffer).split("[\\n\\s\\p{Punct}]+");
        for (int i = 0; i < words.length; i++){
            words[i] = words[i].toLowerCase();
        }
        return words;
    }

    /**
     * Counts word frequency in the String array.
     * @return a Map<String, Integer>:
     *      String - text of the word,
     *      Integer - frequency of the word.
     */
    public Map<String, Integer> getWordFrequency(String[] wordsArray) {
        Map<String, Integer> wordsMap = new HashMap<>();
        for (String s : wordsArray) {
            if (!wordsMap.containsKey(s)) {
                wordsMap.put(s, 1);
            } else {
                Integer frequency = wordsMap.get(s);
                wordsMap.remove(s);
                wordsMap.put(s, ++frequency);
            }
        }
        return wordsMap;
    }

    /**
     * Find the most two frequent words
     * and print them out sorted alphabetically in a reversed order.
     */
    public void getTheMostTwoFrequentWords(String[] wordsArray){
        long time = System.currentTimeMillis();
        Map<String, Integer> wordsMap = getWordFrequency(wordsArray);
        List words = new ArrayList<>(wordsMap.entrySet());
        Collections.sort(words, new Comparator<Map.Entry<String, Integer>>() {
            @Override
            public int compare(Map.Entry<String, Integer> a, Map.Entry<String, Integer> b) {
                return b.getValue() - a.getValue();
            }
        });

        List subList = words.subList(0, 2);
        Collections.sort(subList, new Comparator<Map.Entry<String, Integer>>() {
            @Override
            public int compare(Map.Entry<String, Integer> a, Map.Entry<String, Integer> b) {
                return b.getKey().compareTo(a.getKey());
            }
        });
        subList.forEach(System.out::println);
        time = System.currentTimeMillis() - time;
        System.out.println("elapsed time: " + time + " milis");
    }

    /**
     * Find first three longest words and print this words
     * along with the their length sorted them in a descend order
     * by the total number of letters each word contains.
     */
    public void getFirstThreeLongestWords(String[] wordsArray){
        long time = System.currentTimeMillis();
        List<String> wordsList = new ArrayList<>();
        for (String s : wordsArray) {
            if (!wordsList.contains(s)){
                wordsList.add(s);
            }
        }
        Collections.sort(wordsList, (s1, s2) -> s2.length() - s1.length());
        List<String> subList = wordsList.subList(0, 3);
        subList.forEach(s -> System.out.println(s + " -> " + s.length()));
        time = System.currentTimeMillis() - time;
        System.out.println("elapsed time: " + time + " milis");
    }

    /**
     * Find first three words which have duplicates
     * and print them inversely in the upper case
     * sorted by length in ascending order.
     */
    public void getFirstThreeDuplicates(String[] wordsArray){
        long time = System.currentTimeMillis();
        List<String> wordsList = new ArrayList<>();
        Set<String> wordsSet = new LinkedHashSet<>();
        for (String s : wordsArray) {
            if (!wordsList.contains(s)){
                wordsList.add(s);
            } else {
                if (wordsSet.size() == 3){
                    break;
                } else {
                    wordsSet.add(s);
                }
            }
        }
        wordsList = new ArrayList<>(wordsSet);
        Collections.sort(wordsList, (s1, s2) -> s1.length() - s2.length());
        for (String s : wordsList) {
            s = s.toUpperCase();
            StringBuilder stringBuilder = new StringBuilder(s);
            stringBuilder.reverse();
            System.out.println(stringBuilder);
        }
        time = System.currentTimeMillis() - time;
        System.out.println("elapsed time: " + time + " milis");
    }

}