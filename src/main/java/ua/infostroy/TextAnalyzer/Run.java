package ua.infostroy.TextAnalyzer;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;

import java.util.Scanner;

/**
* The main class. Application entry point.
*/
public class Run {

    @Parameter(names = {"-i", "--input"}, description = "Path to the input file")
    private String input;

    @Parameter(names = {"-t", "--task"}, description = "Task to execute")
    private String task;

    @Parameter(names = "--help", description = "Show help messages")
    private boolean help;

    public void taskExecution() {
        Scanner sc = new Scanner(System.in);
        while (true) {

            String line = sc.nextLine();
            if (line.equals("exit")) {
                sc.close();
                return;
            }

            String[] array = line.split("\\s");
            Run run = new Run();

            try {
                new JCommander(run, array);
            } catch (ParameterException ex) {
                System.err.println("Unknown parameter");
            }

            if (run.help) {
                Help.showMessage();
            }

            String task = run.task;
            if (task == null) {
                continue;
            }

            TextAnalyzer textAnalyzer = new TextAnalyzer();
            String[] wordsArray = textAnalyzer.readFile(run.input);

            try {
                switch (task) {
                    case "frequency":
                        textAnalyzer.getTheMostTwoFrequentWords(wordsArray);
                        break;
                    case "length":
                        textAnalyzer.getFirstThreeLongestWords(wordsArray);
                        break;
                    case "duplicates":
                        textAnalyzer.getFirstThreeDuplicates(wordsArray);
                        break;
                    default:
                        System.err.println("Incorrect task name");
                        break;
                }
            } catch (IndexOutOfBoundsException ex) {
                System.out.print("");
            }

        }
    }

    public static void main(String[] args) {
        new Run().taskExecution();
    }
}