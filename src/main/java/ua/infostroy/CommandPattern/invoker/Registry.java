package ua.infostroy.CommandPattern.invoker;

import ua.infostroy.CommandPattern.command.Command;

import java.util.*;

public class Registry {
    Map<String, Command> commands = new HashMap<String, Command>();

    public void addCommand(String commandName, Command command) {
        commands.put(commandName, command);
    }

    public void executeCommand(String commandName, String[] wordsArray) {
        Command command = commands.get(commandName);
        if (command == null) {
            throw new RuntimeException("Unsupported command: " + commandName);
        }
        command.execute(wordsArray);
    }
}
