package ua.infostroy.CommandPattern.client;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import ua.infostroy.CommandPattern.command.ConcreteCommand.DuplicateWordCommand;
import ua.infostroy.CommandPattern.command.ConcreteCommand.FrequentWordCommand;
import ua.infostroy.CommandPattern.command.ConcreteCommand.LongWordCommand;
import ua.infostroy.CommandPattern.invoker.Registry;
import ua.infostroy.CommandPattern.util.FileUtil;
import ua.infostroy.CommandPattern.util.Help;

import java.util.Scanner;

/**
 * The main class. Application entry point.
 */
public class Run {

    @Parameter(names = {"-i", "--input"}, description = "Path to the input file")
    private String input;

    @Parameter(names = {"-t", "--task"}, description = "Task to execute")
    private String task;

    @Parameter(names = "--help", description = "Show help messages")
    private boolean help;

    public void taskExecution() {
        Registry registry = new Registry();
        registry.addCommand("frequency", new FrequentWordCommand());
        registry.addCommand("length", new LongWordCommand());
        registry.addCommand("duplicates", new DuplicateWordCommand());

        Scanner sc = new Scanner(System.in);

        while (true) {

            String line = sc.nextLine();
            if (line.equals("exit")) {
                sc.close();
                return;
            }

            String[] array = line.split("\\s");
            Run run = new Run();

            try {
                new JCommander(run, array);
            } catch (ParameterException ex) {
                System.err.println("Unknown parameter");
            }

            if (run.help) {
                Help.showMessage();
            }

            String task = run.task;
            if (task == null) {
                continue;
            }

            String[] wordsArray = FileUtil.readFile(run.input);

            try {
                registry.executeCommand(task, wordsArray);
            } catch (IndexOutOfBoundsException ex) {
                System.out.println("");
            } catch (RuntimeException ex) {
                System.err.println(ex.getMessage());
            }

        }
    }

    public static void main(String[] args) {
        new Run().taskExecution();
    }

}
