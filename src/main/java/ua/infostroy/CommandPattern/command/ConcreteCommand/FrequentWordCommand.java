package ua.infostroy.CommandPattern.command.ConcreteCommand;

import ua.infostroy.CommandPattern.command.Command;

import java.util.*;

public class FrequentWordCommand implements Command {

    /**
     * Find the most two frequent words
     * and print them out sorted alphabetically in a reversed order.
     */
    @Override
    public void execute(String[] wordsArray) {
        long time = System.currentTimeMillis();
        Map<String, Integer> wordsMap = getWordFrequency(wordsArray);
        List words = new ArrayList<>(wordsMap.entrySet());
        Collections.sort(words, new Comparator<Map.Entry<String, Integer>>() {
            @Override
            public int compare(Map.Entry<String, Integer> a, Map.Entry<String, Integer> b) {
                return b.getValue() - a.getValue();
            }
        });

        List subList = words.subList(0, 2);
        Collections.sort(subList, new Comparator<Map.Entry<String, Integer>>() {
            @Override
            public int compare(Map.Entry<String, Integer> a, Map.Entry<String, Integer> b) {
                return b.getKey().compareTo(a.getKey());
            }
        });
        subList.forEach(System.out::println);
        time = System.currentTimeMillis() - time;
        System.out.println("elapsed time: " + time + " milis");
    }

    /**
     * Counts word frequency in the String array.
     * @return a Map<String, Integer>:
     *      String - text of the word,
     *      Integer - frequency of the word.
     */
    public Map<String, Integer> getWordFrequency(String[] wordsArray) {
        Map<String, Integer> wordsMap = new HashMap<>();
        for (String s : wordsArray) {
            if (!wordsMap.containsKey(s)) {
                wordsMap.put(s, 1);
            } else {
                Integer frequency = wordsMap.get(s);
                wordsMap.remove(s);
                wordsMap.put(s, ++frequency);
            }
        }
        return wordsMap;
    }

}