package ua.infostroy.CommandPattern.command.ConcreteCommand;

import ua.infostroy.CommandPattern.command.Command;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LongWordCommand implements Command {

    /**
     * Find first three longest words and print this words
     * along with the their length sorted them in a descend order
     * by the total number of letters each word contains.
     */
    @Override
    public void execute(String[] wordsArray) {
        long time = System.currentTimeMillis();
        List<String> wordsList = new ArrayList<>();
        for (String s : wordsArray) {
            if (!wordsList.contains(s)){
                wordsList.add(s);
            }
        }
        Collections.sort(wordsList, (s1, s2) -> s2.length() - s1.length());
        List<String> subList = wordsList.subList(0, 3);
        subList.forEach(s -> System.out.println(s + " -> " + s.length()));
        time = System.currentTimeMillis() - time;
        System.out.println("elapsed time: " + time + " milis");
    }

}
