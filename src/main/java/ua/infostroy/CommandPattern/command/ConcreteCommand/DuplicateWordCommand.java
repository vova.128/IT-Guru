package ua.infostroy.CommandPattern.command.ConcreteCommand;

import ua.infostroy.CommandPattern.command.Command;

import java.util.*;

public class DuplicateWordCommand implements Command {

    /**
     * Find first three words which have duplicates
     * and print them inversely in the upper case
     * sorted by length in ascending order.
     */
    @Override
    public void execute(String[] wordsArray) {
        long time = System.currentTimeMillis();
        List<String> wordsList = new ArrayList<>();
        Set<String> wordsSet = new LinkedHashSet<>();
        for (String s : wordsArray) {
            if (!wordsList.contains(s)){
                wordsList.add(s);
            } else {
                if (wordsSet.size() == 3){
                    break;
                } else {
                    wordsSet.add(s);
                }
            }
        }
        wordsList = new ArrayList<>(wordsSet);
        Collections.sort(wordsList, (s1, s2) -> s1.length() - s2.length());
        for (String s : wordsList) {
            s = s.toUpperCase();
            StringBuilder stringBuilder = new StringBuilder(s);
            stringBuilder.reverse();
            System.out.println(stringBuilder);
        }
        time = System.currentTimeMillis() - time;
        System.out.println("elapsed time: " + time + " milis");
    }

}
