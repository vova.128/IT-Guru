package ua.infostroy.CommandPattern.command;

public interface Command {
    public void execute(String[] wordsArray);
}
