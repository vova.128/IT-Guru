package ua.infostroy.CommandPattern.util;

import java.io.*;

public class FileUtil {

    /**
     * Read the file into the char array.
     * @param path - the path to the file
     * @return String array of words
     */
    public static String[] readFile(String path){
        File file = new File(path);
        char[] buffer = null;
        try{
            buffer = new char[(int)file.length()];
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(new FileInputStream(file), "UTF-8"));
            br.read(buffer);
            br.close();
        } catch (FileNotFoundException ex){
            System.err.println("File " + file.getName() + " wasn't found");
        } catch(IOException ex){
            System.out.println(ex.getMessage());
        }
        String[] words = new String(buffer).split("[\\n\\s\\p{Punct}]+");
        for (int i = 0; i < words.length; i++){
            words[i] = words[i].toLowerCase();
        }
        return words;
    }

}
