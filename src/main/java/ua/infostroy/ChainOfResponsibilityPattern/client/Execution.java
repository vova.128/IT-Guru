package ua.infostroy.ChainOfResponsibilityPattern.client;

import ua.infostroy.ChainOfResponsibilityPattern.handler.ConcreteHandler.ExtensionHandler;
import ua.infostroy.ChainOfResponsibilityPattern.handler.ConcreteHandler.NameHandler;
import ua.infostroy.ChainOfResponsibilityPattern.handler.ConcreteHandler.SizeHandler;
import ua.infostroy.ChainOfResponsibilityPattern.handler.Handler;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Execution {

    public void execute() {
        Scanner sc = new Scanner(System.in);
        while (true) {

            Handler initialHandler = null;
            Handler extensionHandler = null;
            Handler sizeHandler = null;
            Handler nameHandler = null;

            System.out.println("Type the directory path:");
            String directoryPath = sc.nextLine();

            File directory = new File(directoryPath);
            if (!directory.isDirectory()) {
                System.err.println("Oops... It is not a directory\n");
                continue;
            }

            File[] files = directory.listFiles();
            if (files != null && files.length == 0) {
                System.err.println("Oops... The directory is empty\n");
                continue;
            }

            System.out.println("Type the beginning of the filename:");
            String fileName = sc.nextLine();

            System.out.println("Type the minimum file size in bytes:");
            String minFileSize = sc.nextLine();

            System.out.println("Type the maximum file size in bytes:");
            String maxFileSize = sc.nextLine();

            System.out.println("Type the file extension:");
            String extension = sc.nextLine();

            if (!extension.isEmpty()) {
                extensionHandler = new ExtensionHandler(null, extension);
            }

            if (!minFileSize.isEmpty() && !maxFileSize.isEmpty()) {
                long minSize = Long.parseLong(minFileSize);
                long maxSize = Long.parseLong(maxFileSize);
                if (minSize > maxSize) {
                    System.err.println("Oops... Minimum file size is greater than the maximum\n");
                    continue;
                }
                sizeHandler = new SizeHandler(extensionHandler, minSize, maxSize);
            }

            if (!fileName.isEmpty()) {
                nameHandler = new NameHandler(sizeHandler, fileName);
            }

            if (nameHandler != null) {
                initialHandler = nameHandler;
            } else if (sizeHandler != null) {
                initialHandler = sizeHandler;
            } else if (extensionHandler != null) {
                initialHandler = extensionHandler;
            }

            List<File> listFiles = new ArrayList<>();
            for (File file : files) {
                if (!file.isDirectory()) {
                    listFiles.add(file);
                }
            }

            List<String> resultingFiles = new ArrayList<>();

            System.out.println("Search results:");

            for (File file : listFiles) {
                if (initialHandler.accept(file)) {
                    resultingFiles.add(file.getName());
                }
            }

            resultingFiles.forEach(System.out::println);

            if (sc.nextLine().equals("exit")) {
                sc.close();
                return;
            }

        }
    }

    public static void main(String[] args) {
        new Execution().execute();
    }
}
