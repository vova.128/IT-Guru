package ua.infostroy.ChainOfResponsibilityPattern.handler.ConcreteHandler;

import ua.infostroy.ChainOfResponsibilityPattern.handler.Handler;

import java.io.File;

public class SizeHandler extends Handler {

    private long minSize;
    private long maxSize;

    public SizeHandler(Handler handler, long minSize, long maxSize) {
        this.handler = handler;
        this.minSize = minSize;
        this.maxSize = maxSize;
    }

    @Override
    public boolean accept(File file) {
        return file.length() >= minSize && file.length() <= maxSize
                && (handler == null || handler.accept(file));
    }
    
}

