package ua.infostroy.ChainOfResponsibilityPattern.handler.ConcreteHandler;

import ua.infostroy.ChainOfResponsibilityPattern.handler.Handler;

import java.io.File;

public class NameHandler extends Handler {

    private String name;

    public NameHandler(Handler handler, String name) {
        this.handler = handler;
        this.name = name;
    }

    @Override
    public boolean accept(File file) {
        return file.getName().startsWith(name) &&
                (handler == null || handler.accept(file));
    }

}
