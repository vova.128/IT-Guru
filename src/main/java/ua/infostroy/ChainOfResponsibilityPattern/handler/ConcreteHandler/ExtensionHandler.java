package ua.infostroy.ChainOfResponsibilityPattern.handler.ConcreteHandler;

import com.google.common.io.Files;
import ua.infostroy.ChainOfResponsibilityPattern.handler.Handler;

import java.io.File;

public class ExtensionHandler extends Handler {

    private String extension;

    public ExtensionHandler(Handler handler, String extension) {
        this.handler = handler;
        this.extension = extension;
    }

    @Override
    public boolean accept(File file) {
        return extension.equals(Files.getFileExtension(file.getName())) &&
                (handler == null || handler.accept(file));
    }

}
