package ua.infostroy.ChainOfResponsibilityPattern.handler;

import java.io.File;

public abstract class Handler {
    protected Handler handler;
    public abstract boolean accept(File file);
}
